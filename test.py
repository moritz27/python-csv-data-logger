from random import seed
from random import random
# seed random number generator
seed(1)

from data_logger.data_logger import DATA_LOGGER_CLASS

header = ("longitude", "latitude", "signalstrength")
path = "logs/"
logger = DATA_LOGGER_CLASS(header)

try:
    for i in range (0,100):
        linedata = (random(), random(), random())
        logger.write(linedata)

finally:
    logger.close()


